package com.dam.tfg.can_eat.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.dam.tfg.can_eat.main.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class añadirController implements Initializable {

	@FXML
	private Text paneTitle;

	@FXML
	private Text labelMarca;

	@FXML
	private Text labelAlergenos;

	@FXML
	private ListView<?> listAlergenos;

	@FXML
	private TextArea textDescripcion;

	@FXML
	private TextField textNombre;

	@FXML
	private TextField textMarca;

	@FXML
	void onActionAñadir(ActionEvent event) {
		//TOOD: añadir datos en la BD
		//TODO: Comprobar datos
		//TODO: Añadir JOptionPane para indicar errores
	}

	@FXML
	void onActionCancelar(ActionEvent event) {
		Stage stage = (Stage)( (Node) event.getSource()).getScene().getWindow();
		stage.close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		paneTitle.setText(Main.NAME_PANE);
		switch (Main.NAME_PANE) {
		case "ALERGENO":
			//ReSize elementos
			textNombre.setPrefWidth(450);
			textDescripcion.setPrefHeight(190);
			//Ocultar Items no necesarios
			labelAlergenos.setVisible(false);
			listAlergenos.setVisible(false);
			labelMarca.setVisible(false);
			textMarca.setVisible(false);
			break;
		case "MARCA":
			//ReSize elementos
			textNombre.setPrefWidth(450);
			textDescripcion.setPrefHeight(190);
			//Ocultar Items no necesarios
			labelAlergenos.setVisible(false);
			listAlergenos.setVisible(false);
			labelMarca.setVisible(false);
			textMarca.setVisible(false);
			break;

		case "PRODUCTO":

			break;

		default:
			JOptionPane.showMessageDialog(null, "1010: Pane desconocido", "Error",
					JOptionPane.WARNING_MESSAGE);
			System.exit(1);
			break;
		}

	}

}
