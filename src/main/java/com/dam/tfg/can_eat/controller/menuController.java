package com.dam.tfg.can_eat.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.dam.tfg.can_eat.hibernate.controller.HibernateUtil;
import com.dam.tfg.can_eat.hibernate.modelo.Alergeno;
import com.dam.tfg.can_eat.hibernate.modelo.Marca;
import com.dam.tfg.can_eat.hibernate.modelo.Producto;
import com.dam.tfg.can_eat.main.Main;

import antlr.debug.Event;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

//TODO: Hacer todos los elemento responsive
public class menuController implements Initializable {

	// PaneProductos
	@FXML
	private Pane paneProductos;

	@FXML
	private TableView<Producto> ListProductos;

	@FXML
	private TableColumn<Producto, Integer> columnIdProducto;

	@FXML
	private TableColumn<Producto, String> columnNombreProducto;

	@FXML
	private TableColumn<Producto, String> columnDescripcionProducto;

	@FXML
	private TextField textBuscadorProductos;

	@FXML
	void onActionAñadirProductos(ActionEvent event) throws IOException {
		showStage("/view/AñadirView.fxml");
	}

	@FXML
	void onActionBuscarProductos(ActionEvent event) {

	}

	@FXML
	void onActionEliminarProductos(ActionEvent event) {

	}

	@FXML
	void onActionModificarProductos(ActionEvent event) {

	}

	@FXML
	void accionProducto(ActionEvent event) {
		Main.NAME_PANE = "PRODUCTO";
		paneProductos.toFront();
	}

	// Pane Marcas
	@FXML
	private Pane paneMarcas;

	@FXML
	private TableView<Marca> ListMarcas;

	@FXML
	private TableColumn<Marca, Integer> columnIdMarca;

	@FXML
	private TableColumn<Marca, String> columnNombreMarca;

	@FXML
	private TableColumn<Marca, String> columnDescripcionMarca;

	@FXML
	private TextField textBuscadorMarcas;

	@FXML
	void onActionAñadirMarcas(ActionEvent event) throws IOException {
		showStage("/view/AñadirView.fxml");
	}

	@FXML
	void onActionBuscarMarcas(ActionEvent event) {

	}

	@FXML
	void onActionEliminarMarcas(ActionEvent event) {

	}

	@FXML
	void onActionModificarMarcas(ActionEvent event) {

	}

	@FXML
	void actionMarca(ActionEvent event) {
		Main.NAME_PANE = "MARCA";
		paneMarcas.toFront();
	}

	// Pane Alergeno
	@FXML
	private Pane paneAlergeno;

	@FXML
	private TableView<Alergeno> ListAlergenos;

	@FXML
	private TableColumn<Alergeno, Integer> columnIdAlergeno;

	@FXML
	private TableColumn<Alergeno, String> columnNombreAlergeno;

	@FXML
	private TableColumn<Alergeno, String> columnDescripcionAlergeno;

	@FXML
	private TextField textBuscadorAlergenos;

	@FXML
	void onActionAñadirAlergenos(ActionEvent event) throws IOException {
		showStage("/view/AñadirView.fxml");

	}

	@FXML
	void onActionBuscarAlergenos(ActionEvent event) {

	}

	@FXML
	void onActionEliminarAlergenos(ActionEvent event) {

	}

	@FXML
	void onActionModificarAlergenos(ActionEvent event) {

	}

	@FXML
	void accionAlergeno(ActionEvent event) {
		Main.NAME_PANE = "ALERGENO";
		paneAlergeno.toFront();
	}

	// Logout
	@FXML
	void accionLogout(ActionEvent event) {
		Platform.exit();
		System.exit(0);

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loadTableAlergenos();
		loadTableProductos();
		loadTableMarcas();

	}

	// Metodos
	private void loadTableAlergenos() {
		// TODO añadir JOptionPane "Pensando"
		List<Alergeno> alergenos = HibernateUtil.getAlergeno();
		ObservableList<Alergeno> obsAlerg = FXCollections.observableArrayList();

		for (Alergeno a : alergenos) {
			obsAlerg.add(a);
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdAlergeno.setCellValueFactory(new PropertyValueFactory<>("IdAlergeno"));
		columnNombreAlergeno.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionAlergeno.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListAlergenos.setItems(obsAlerg);

	}

	private void loadTableProductos() {
		// TODO añadir JOptionPane "Pensando"
		List<Producto> productos = HibernateUtil.getProducto();
		ObservableList<Producto> obsProduct = FXCollections.observableArrayList();

		for (Producto a : productos) {
			obsProduct.add(a);
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdProducto.setCellValueFactory(new PropertyValueFactory<>("IdProducto"));
		columnNombreProducto.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionProducto.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListProductos.setItems(obsProduct);

	}

	private void loadTableMarcas() {
		// TODO añadir JOptionPane "Pensando"
		List<Marca> marcas = HibernateUtil.getMarca();
		ObservableList<Marca> obsMarca = FXCollections.observableArrayList();

		for (Marca a : marcas) {
			obsMarca.add(a);
			// System.out.println(a.toString());
		}
		// Selecionr getter para la cada columna
		columnIdMarca.setCellValueFactory(new PropertyValueFactory<>("IdMarca"));
		columnNombreMarca.setCellValueFactory(new PropertyValueFactory<>("Nombre"));
		columnDescripcionMarca.setCellValueFactory(new PropertyValueFactory<>("Descripcion"));

		// aplicar la ObservableList en la tableview
		ListMarcas.setItems(obsMarca);

	}

	private void showStage(String view) throws IOException {
		// Abrir nuevo stage
		Parent newRoot = FXMLLoader.load(getClass().getResource(view));
		Stage stage2 = new Stage();
		stage2.setScene(new Scene(newRoot));
		
		//Holdear Stage principal
		stage2.initOwner(paneAlergeno.getScene().getWindow());
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.resizableProperty().setValue(false);
		
		stage2.show();
	}

}