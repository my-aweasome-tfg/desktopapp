package com.dam.tfg.can_eat.main;



import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
	//Guarda el nombre del Pane en el que estoy para las opciones (añadir, modificar)
	public static String NAME_PANE = "ALERGENO";

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/view/menu.fxml"));
		Scene scene = new Scene(root);
		
		//...
		primaryStage.getIcons().add(new Image("/img/icono.png"));
		primaryStage.setTitle("Gestión: Can I eat It ");
		
		//TODO Eliminar Windows Bar y añadir toolbar personalizada
		//primaryStage.initStyle(StageStyle.TRANSPARENT);
		
		//Deshabilitar maximizar
		primaryStage.resizableProperty().setValue(false);
		//...
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}

	
	public static void main(String[] args)  {
		launch(args);

	}

}
