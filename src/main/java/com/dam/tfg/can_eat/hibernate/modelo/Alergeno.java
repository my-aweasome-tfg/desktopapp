package com.dam.tfg.can_eat.hibernate.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "ALERGENO")
public class Alergeno {
	@Id
	@Column(name="ID")
	private int idAlergeno;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="DESCRIPCION")
	private String descripcion;
	
	public Alergeno () {}

	public Alergeno(int idAlergeno, String nombre, String descripcion) {
		this.idAlergeno = idAlergeno;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public int getIdAlergeno() {
		return idAlergeno;
	}

	public void setIdAlergeno(int idAlergeno) {
		this.idAlergeno = idAlergeno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Alergeno [idAlergeno=" + idAlergeno + ", nombre=" + nombre + ", descripcion=" + descripcion + "]";
	}
	
	
	
}
