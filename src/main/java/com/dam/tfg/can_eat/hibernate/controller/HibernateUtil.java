package com.dam.tfg.can_eat.hibernate.controller;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.dam.tfg.can_eat.hibernate.modelo.*;

public class HibernateUtil {
	public static EntityManager manager;
	public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence");
	
	//TODO funcion para filtrar?
	public static List<Alergeno> getAlergeno() {
		List<Alergeno> list = new ArrayList<Alergeno>();

		manager = emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Alergeno> alergenos = (List<Alergeno>) manager.createQuery("FROM Alergeno").getResultList();
		for (Alergeno a : alergenos) {
			list.add(a);
		}
		manager.close();

		return list;
	}
	
	
	public static List<Marca> getMarca() {
		List<Marca> list = new ArrayList<Marca>();

		manager = emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Marca> marcas = (List<Marca>) manager.createQuery("FROM Marca").getResultList();
		for (Marca a : marcas) {
			list.add(a);
		}
		manager.close();

		return list;
	}
	
	public static List<Producto> getProducto() {
		List<Producto> list = new ArrayList<Producto>();

		manager = emf.createEntityManager();
		@SuppressWarnings("unchecked")
		List<Producto> productos = (List<Producto>) manager.createQuery("FROM Producto").getResultList();
		for (Producto a : productos) {
			list.add(a);
		}
		manager.close();

		return list;
	}
}
